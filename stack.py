import math
import sys 
import random

def pair(x,y,m):
    if x > m : 
        return 
    return y*m+x 

def unpair(r,m):
    return r%m,r//m 

def bits(x):
    if(x == 0):
        return 1
    return int(math.log2(x))+1

def list_size(list):
    res = 0
    for e in list : 
        res += sys.getsizeof(e)

    return res
class stack : 

    # it represent the four stacks. 
    results = [0,0,0,0]

    def push(self,x):
        x += 2 # to fix the problem that arises when pushing 0 or 1, as their bits function is not equal to 2.
        
        for i,y in enumerate(self.results) : 
            m = 2**bits(x)
            result = pair(x,y,m)
            self.results[i] = result
            x = bits(x)

    def pop(self):
        x = 2
        for i in range(len(self.results)-1,-1,-1):
            r = self.results[i]
            m = 2**x
            x,r = unpair(r,m)
            self.results[i] = r

        return x-2    



my_stack = stack()
normal_stack = []
n = 10000
biggest_number = 10000
for i in range(n):
    r = random.randint(0,biggest_number)
    my_stack.push(r)
    normal_stack.append(r)

print("normal_stack size:",list_size(normal_stack),'Bytes')
print("my_stack size    :",list_size(my_stack.results),'Bytes')

credebility = 0
for i in range(n-1,-1,-1): 
    if normal_stack[i] == my_stack.pop():
        credebility += 1 

if credebility == n : 
    print("OK")
else : 
    print("KO")